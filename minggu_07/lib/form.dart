import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: "Belajar Form FLutter",
    home: BelajarForm(),
  ));
}

class BelajarForm extends StatefulWidget {
  @override
  _BelajarFormState createState() => _BelajarFormState();
}

class _BelajarFormState extends State<BelajarForm> {
  final _formKey = GlobalKey<FormState>();
  double nilaiSlider = 1;
  bool nilaiCheckBox = false;
  bool nilaiSwitch = true;

  TextEditingController Name = new TextEditingController();
  TextEditingController Pwd = new TextEditingController();

  void kirimData() {
    AlertDialog alertDialog = AlertDialog(
      content: Container(
        height: 200.0,
        child: Column(
          children: <Widget>[
            Text("Nama lengkap : ${Name.text}"),
            Text("Password : ${Pwd.text}"),
            RaisedButton(
              child: new Text("OK"),
              onPressed: () => Navigator.pop(context),
              color: Colors.teal,
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("belajar Form Flutter"),
      ),
      body: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              children: [
                TextFormField(
                  controller: Name,
                  decoration: InputDecoration(
                      hintText: "ex : rianezza012",
                      labelText: "Name",
                      icon: Icon(Icons.people),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5.0))),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Field cannot be empty";
                    }
                    return null;
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextFormField(
                    controller: Pwd,
                    obscureText: true,
                    decoration: InputDecoration(
                        labelText: "Password",
                        icon: Icon(Icons.security),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(5.0))),
                    validator: (value) {
                      if (value!.isEmpty) {
                        return "Field cannot be empty";
                      }
                      return null;
                    },
                  ),
                ),
                CheckboxListTile(
                    title: Text('Belajar Dasar FLutter'),
                    subtitle: Text('Dart, widget, http'),
                    value: nilaiCheckBox,
                    activeColor: Colors.deepPurpleAccent,
                    onChanged: (value) {
                      setState(() {
                        nilaiCheckBox = value!;
                      });
                    }),
                SwitchListTile(
                  title: Text('Backend Programming'),
                  subtitle: Text('Dart, NodeJS, PHP, Java, dll'),
                  activeTrackColor: Colors.pink[100],
                  activeColor: Colors.red,
                  value: nilaiSwitch,
                  onChanged: (value) {
                    setState(() {
                      nilaiSwitch = value;
                    });
                  },
                ),
                Slider(
                  value: nilaiSlider,
                  min: 0,
                  max: 100,
                  onChanged: (value) {
                    setState(() {
                      nilaiSlider = value;
                    });
                  },
                ),
                RaisedButton(
                  child: Text(
                    "Submit",
                    style: TextStyle(color: Colors.white),
                  ),
                  color: Colors.blue,
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      kirimData();
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
