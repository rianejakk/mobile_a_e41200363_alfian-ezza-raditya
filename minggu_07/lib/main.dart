import 'package:flutter/material.dart';
import 'form.dart';

void main() {
  runApp(MaterialApp(
    home: Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _formKey = GlobalKey<FormState>();

  List<String> carBrands = [
    "Nissan",
    "Honda",
    "Suzuki",
    "Daihatsu",
    "Toyota",
    "Subaru",
    "Mazda"
  ];

  String _carBrands = "Nissan";
  String _jk = "";

  TextEditingController controllerNama = new TextEditingController();
  TextEditingController controllerPass = new TextEditingController();
  TextEditingController controllerMoto = new TextEditingController();

  @override
  void _pilihJK(String value) {
    setState(() {
      _jk = value;
    });
  }

  @override
  void pilihCarBrand(String value) {
    setState(() {
      _carBrands = value;
    });
  }

  void kirimData() {
    AlertDialog alertDialog = AlertDialog(
      content: Container(
        height: 200.0,
        child: Column(
          children: <Widget>[
            new Text("Nama lengkap : ${controllerNama.text}"),
            new Text("Password : ${controllerPass.text}"),
            new Text("Moto Hidup : ${controllerMoto.text}"),
            new Text("Car Brand : ${_carBrands}"),
            new Text("Jenis Kelamin : ${_jk}"),
            new RaisedButton(
              child: new Text("OK"),
              onPressed: () => Navigator.pop(context),
              color: Colors.teal,
            )
          ],
        ),
      ),
    );
    showDialog(context: context, builder: (_) => alertDialog);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Icon(Icons.list),
        title: Text("Data Diri"),
        backgroundColor: Colors.teal,
      ),
      body: ListView(
        key: _formKey,
        children: [
          Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                TextFormField(
                  controller: controllerNama,
                  decoration: InputDecoration(
                      hintText: "Nama Lengkap",
                      labelText: "Nama Lengkap",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                  validator: (value) {
                    if (value!.isEmpty) {
                      return "Field cannot be empty";
                    }
                    return "";
                  },
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                TextField(
                  controller: controllerPass,
                  obscureText: true,
                  decoration: InputDecoration(
                      hintText: "Password",
                      labelText: "Password",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                TextField(
                  controller: controllerMoto,
                  maxLines: 3,
                  decoration: InputDecoration(
                      hintText: "Moto Hidup",
                      labelText: "Moto Hidup",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(20.0))),
                ),
                Padding(padding: new EdgeInsets.only(top: 20.0)),
                RadioListTile(
                  value: "Pria",
                  title: new Text("Pria"),
                  groupValue: _jk,
                  onChanged: (String? value) {
                    _pilihJK(value!);
                  },
                  activeColor: Colors.blue,
                  subtitle: new Text("Pilih ini jika anda pria"),
                ),
                RadioListTile(
                  value: "Wanita",
                  title: new Text("Wanita"),
                  groupValue: _jk,
                  onChanged: (String? value) {
                    _pilihJK(value!);
                  },
                  activeColor: Colors.blue,
                  subtitle: new Text("Pilih ini jika anda wanita"),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                ),
                Row(
                  children: <Widget>[
                    Text(
                      "Car Brands",
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.blue,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 15.0),
                    ),
                    DropdownButton(
                      value: _carBrands,
                      items: carBrands.map((String value) {
                        return new DropdownMenuItem(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                      onChanged: (String? value) {
                        pilihCarBrand(value!);
                      },
                    )
                  ],
                ),
                RaisedButton(
                    child: Text("OK"),
                    color: Colors.blue,
                    onPressed: () {
                      // if (_formKey.currentState!.validate()) {}
                      // else {
                        kirimData();
                        // }
                      ;
                    }),
                RaisedButton(
                  child: Text("Form Page"),
                  color: Colors.blue,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => BelajarForm()),
                    );
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
