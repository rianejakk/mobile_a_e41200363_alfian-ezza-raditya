import 'dart:io';

void main() {
  String role;
  print("Enter Your Name : ");
  String name = stdin.readLineSync()!;
  if (name == "") {
    print("Name cannot be empty");
  } else {
    print(
        "Pilih peranmu untuk memulai game :\n1. Witch\n2. Guard\n3. Werewolf");
    role = stdin.readLineSync()!;
    role == "1"
        ? role = "Witch"
        : role == "2"
            ? role = "Guard"
            : role == "3"
                ? role = "Werewolf"
                : print("Option unavailable");
    print("You are " + name + " with the role of " + role);
  }
}
