import 'dart:io';

void main() {
  print("Day of week : ");
  String day_of_week = stdin.readLineSync()!;

  switch (day_of_week) {
    case "1":
      print("Senin\nWhy do you have asthma? There's oxygen everywhere!");
      break;
    case "2":
      print("Selasa\nWhoever said pen is mightier than sword never felt what's like being stabbed by one.");
      break;
    case "3":
      print("Rabu\nGatherings are just being alone together.");
      break;
    case "4":
      print("Kamis\nSome people cannot accept change, hence they stuck.");
      break;
    case "5":
      print("Jumat\nYou are not a protagonist in this world.");
      break;
    case "6":
      print("Sabtu\nHumans are evil to themselves, aren't they?");
      break;
    case "7":
      print("Minggu\nFeeling like do nothing today?");
      break;
    default:
      print("Day do not exist, like your future partner does.");
  }
}
