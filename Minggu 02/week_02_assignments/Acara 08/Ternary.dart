import 'dart:io';

void main() {
  print("This will install Dart into your computer. Y/N?");
  String answer = stdin.readLineSync()!;
  answer == "Y"
      ? print("Dart will be installed shortly.")
      : answer == "N"
          ? print("Operation Aborted")
          : print("Cannot find response");
}
