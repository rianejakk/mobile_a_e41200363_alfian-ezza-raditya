import 'bangun_datar.dart';

class segitiga extends bangun_datar {
  double a = 0;
  double t = 0;
  double b = 0;

  @override
  area() {
    return a * t / 2;
  }

  @override
  perimeter() {
    return a + b + t;
  }
}
