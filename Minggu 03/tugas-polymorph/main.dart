import 'lingkaran.dart';
import 'persegi.dart';
import 'segitiga.dart';

void main() {
  lingkaran circle = new lingkaran();
  persegi square = new persegi();
  segitiga triangle = new segitiga();

  circle.r = 10;
  square.s = 5;

  triangle.a = 10;
  triangle.t = 15;
  triangle.b = 12;

  print(circle.perimeter());
  print(circle.area());

  print(square.perimeter());
  print(square.area());

  print(triangle.perimeter());
  print(triangle.area());
}
