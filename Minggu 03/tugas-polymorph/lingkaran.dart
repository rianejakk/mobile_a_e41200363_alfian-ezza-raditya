import 'bangun_datar.dart';

class lingkaran extends bangun_datar {
  int r = 0;

  @override
  area() {
    return 3.14 * r * r;
  }

  @override
  perimeter() {
    return 3.14 * r * 2;
  }
}
