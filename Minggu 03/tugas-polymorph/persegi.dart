import 'bangun_datar.dart';

class persegi extends bangun_datar {
  int s = 0;

  @override
  area() {
    return s * s;
  }

  @override
  perimeter() {
    return s * 4;
  }
}
